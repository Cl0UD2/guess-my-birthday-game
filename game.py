from random import randint

name = input("Hi! What is your name? ")

month_number = randint(1, 12)
year_number = randint(1924, 2004)

for guess_number in range(1, 6):

    print("guess 1 :", name, "were you born in",
        month_number, "/", year_number, "?")
    response = input("yes or no? ")

    if response == "yes":
        print("i knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good Bye.")
    else:
        print(" Dang! Lemme try again")
